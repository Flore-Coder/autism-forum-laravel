@extends('layouts.app')

@section('content')
    <div class="container col-md8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="section-title">
                <h2>Catégories</h2>
                <p class="mt-3">Dès maintenant, créez votre prochain catégorie de forum en quelques clics !</p>
                <a href="{{ route('categories.create') }}" class="primary-btn mt-3">
                    <i class="fas fa-plus"></i>
                    Nouveau forum
                </a>
            </div>


            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                    <th>
                        Titre
                    </th>
                    <th>
                        Créé le
                    </th>
                    <th>
                        Description
                    </th>

                    <th>
                        Edit
                    </th>
                    <th>
                        Delete
                    </th>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->title }}</td>
                            <td class="bi-text">
                                <span><i class="fa fa-clock-o"></i>{{ $category->created_at }}</span>
                            </td>
                            <td class="bi-text">
                                <a  class="btn font-weight-bold" href="#">{{ $category->description}}</a>
                            </td>
                            <td>
                                <a href="{{ route('categories.edit', ['category' => $category->id]) }}" class="btn ntn-xs btn-info">Modifier</a>
                            </td>

                            <td>
                                <form action="{{ route('categories.destroy', ['category' => $category->id]) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-xs btn-danger" type="submit">Supprimer</button>
                                </form>
                                <!--   <a href="{ { 'categories.edit', ['category' => $category->id]) }}" class="btn ntn-xs btn-info">EDIT</a> -->
                            </td>
                            <td>
                                <!--  <a href="{ { 'categories.destroy', ['category' => $category->id]) }}" class="btn ntn-xs btn-danger">SUPPRIMER</a> -->
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="row">
                @foreach($categories  as $category)
                    <div class="col-12">
                        <div class="card-body {{ $category->color == "" }} ? NULL : style="background= {{ $category->color }};">
                            <div class="bi-text">
                                <h5><a class="btn font-weight-bold" href="#">{{ $category->title }}</a></h5>
                                <span><i class="fa fa-clock-o"></i>{{ $category->created_at }}</span>
                            </div>
                            <div class="bi-text">
                                <a class="btn font-weight-bold" href="#">{{ $category->description}}</a>
                                <span><i class="fa fa-clock-o"></i>{{ $category->created_at }}</span>
                            </div>

                        </div>

                        <div>
                            <!--              @ foreach($threads  as $thread)
                                              <div class="forum-thread">
                                                  <a class="btn font-weight-bold" href="#">{ { $thread->title} }</a>

                                              </div>
                                              @ endfor
                                                  -->
                            <!-- Mettre les premiers fils de discussions -->

                            <div>
                                <!--                <a href="{ { route('thread.index', $category->id) }}" class="primary-btn">-->
                                <!--                <a href="{ { route('thread.index', [ 'category' => $category->id]) }}" class="primary-btn">-->
                                <i class="fas fa-open"></i>
                                Entrer


                                <!--             </a>   </a>-->

                                <!--        <div class="blog-item set-bg" data-setbg="/storage/categories/{ { $category->user_id }}/{ { $category->image }}">
                                            </div>-->


                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection


