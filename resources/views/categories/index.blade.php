@extends('layouts.app')

@section('content')
<div class="container">
    <div>
        <h2>- Forum -</h2>
        <p class="mt-3">Dès maintenant, créez votre forum en quelques clics !</p>
        <a href="{{ route('categories.create') }}" class="primary-btn mt-3">
            <i class="fas fa-plus"></i>
            Nouveau forum
        </a>
    </div>
<!--  -->
    <div class="row">
        <div class="col-md8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Catégories</div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                        <th>
                            Titre
                        </th>
                        <th>
                            Créé le
                        </th>
                        <th>
                            Description
                        </th>

                        <th>
                            Edit
                        </th>
                        <th>
                            Delete
                        </th>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->title }}</td>
                                <td class="bi-text">
                                    <span><i class="fa fa-clock-o"></i>{{ $category->created_at }}</span>
                                </td>
                                <td class="bi-text">
                                    <a href class="btn font-weight-bold" href="#">{{ $category->description}}</a>
                                </td>
                                <td>
                                    <form action="{{ route('categories.edit', ['category' => $category->id]) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button class="btn btn-xs btn-info" type="submit">Modifier</button>
                                    </form>
                                    <!--   <a href="{ { 'categories.edit', ['category' => $category->id]) }}" class="btn ntn-xs btn-info">EDIT</a> -->
                                </td>

                                <td>
                                    <form action="{{ route('categories.destroy', ['category' => $category->id]) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-xs btn-danger" type="submit"></button>
                                    </form>
                                 <!--   <a href="{ { 'categories.edit', ['category' => $category->id]) }}" class="btn ntn-xs btn-info">EDIT</a> -->
                                </td>
                                <td>
                                  <!--  <a href="{ { 'categories.destroy', ['category' => $category->id]) }}" class="btn ntn-xs btn-danger">SUPPRIMER</a> -->
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<!-- -->

    <div class="row">
        @foreach($categories  as $category)
            <div class="col-8">
                <div class="card" style="background:{{ $category->color }}; border: 2px solid #0461B6;">
                    <h5 class="card-title">{{ $category->title }}</h5>
                    <div class="card-body">
                        <div class="bi-text">
                            <span><i class="fa fa-clock-o"></i>{{ $category->created_at }}</span>
                        </div>
                        <div class="bi-text">
                            <a class="btn font-weight-bold" href="#">{{ $category->description}}</a>
                            <span><i class="fa fa-clock-o"></i>{{ $category->created_at }}</span>
                        </div>
                    </div>
                </div>
    <!--        <div class="blog-item set-bg" data-setbg="/storage/categories/{ { $category->user_id }}/{ { $category->image }}">
                </div>-->
                <div class="btn-actions d-flex justify-content-center">
                    <form action="{{ route('category.destroy' ['category' => $category]) }}categories destroy" class="">
                        <!--                <a href="{ { route('thread.index', $category->id) }}" class="primary-btn">-->
                        <i class="fas fa-edit"></i>
                        Modifier
                        <!--                </a>--></form>

                </div>

            </div>
        @endforeach
    </div>
</div>
@endsection

