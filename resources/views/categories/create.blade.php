
@extends('layouts.app')

@section('content')

<section class="contact-from-section spad">
    <div class="container">
        <div class="d-flex justify-content-center">
            <div class="col-lg-8">
                <form action="{{ route('categories.store') }}" class="comment-form contact-form" method="POST" enctype="multipart/form-data">
        <div class="panel-heading">Créer une catégorie</div>
                    @csrf
                    @method('POS c.T')
                    <div class="row">
                        <div class="col-lg-12">
                            <label for="title">Titre</label>
                            <input type="text" placeholder="Le titre de la catégorie de forum" name="title">
                        </div>
                        <div class="col-lg-12">
                            <label for="subtitle">Description du forum</label>
                            <input type="text" placeholder="Description rapide de l'objet du forum" name="description">
                        </div>
                        <div class="form-group">
                            <label for="color">Choisir la couleur de fond du forum</label>
                            <select class="form-control" id="color" name="color">
                                @foreach ($colornames as $rgb => $couleur)
                                    <option value="{{ $rgb }}" style="background-color: {{ $rgb }};">{{ $couleur }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-lg-12">
                            <label for="isPrivate">Privé</label>
                            <input type="radio" id="isPrivate" name="isPrivate"  value="1" checked="true" />
                            <label for="isPrivate"></label>
                            <input hidden type="radio" id="isPrivate" name="isPrivate"  value="0" checked="false" />
                        </div>


                        <div class="text-center col-lg-12">
                            <button type="submit" class="primary-btn">
                                <i class="fas fa-save"></i>
                                Sauvegarder
                            </button>

                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection
