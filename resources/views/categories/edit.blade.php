@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Modifier une catégorie {{ $category->title }}</div>
                    <div class="panel-body">
                        <form action="{{ route('categories.update', $category->id) }}" class="action" method = "post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                    <label for="title">Titre du forum</label>
                                    <input type="text" id="title" name= "title" value="{{ $category->title }}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="description" style="vertical-align: top;">Description du contenu du forum</label>
                                <textarea type="textarea" name="description" rows="4" cols="50">{{ $category->description }} </textarea>
                            </div>
                            <div class="form-group">
                                <label for="color" >Choisir la couleur de fond du forum</label>
                                <select class="form-control" id="color" name="color">
                                    @foreach ($colornames as $rgb => $couleur)
                                        <option value="{{ $rgb }}"  {{ $category->color == $rgb ? 'selected' : '' }} style="background-color:{{ $rgb }};" >{{ $couleur }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="isPrivate">Privé</label>
                                <input type="radio" id="isPrivate" name="isPrivate"  value="1" checked="true" />
                                <label for="isPrivate"></label>
                                <input hidden type="radio" id="isPrivate" name="isPrivate"  value="0" checked="false" />
                            </div>

                            <div class="form-group">
                                <div class="text-center">
                                    <button class="btn btn-success" type= "submit">
                                        Mettre à jour
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

