<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumMessage extends Model
{
    protected $fillable = ['content', 'user_id', 'discussion_id'];


    public function thread()
    {
        return $this->belongsTo('App\ForumThread');
    }

}
