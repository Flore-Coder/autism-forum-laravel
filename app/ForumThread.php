<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumThread extends Model
{
  protected $fillable = ['title', 'content', 'user_id', 'category_id'];

  public function messages()
  {
      return $this->hasMany('App\ForumMessage');
  }

  public function category()
  {
      return $this->belongsTo('App\Category');
  }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
