<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Auth;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        if(Auth::check() && Auth::user()->role_id ==1) {
            $this->redirectTo = route('administrator.dashboard');
        }
        elseif(Auth::check() && Auth::user()->role_id == 2){
            $this->redirectTo = route('redactor.dashboard');
        }
        elseif(Auth::check() && Auth::user()->role_id == 3){
            $this->redirectTo = route('moderator.dashboard');
        }
        elseif(Auth::check() && Auth::user()->role_id == 4){
            $this->redirectTo = route('standard-user');
        }
        $this->middleware('guest')->except('logout');
    }
}
