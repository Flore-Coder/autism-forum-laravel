<?php

namespace App\Http\Controllers\StandardUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StandardUserController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('standard-user/dashboard');
    }

}
