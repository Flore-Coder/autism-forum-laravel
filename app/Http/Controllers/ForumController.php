<?php

namespace App\Http\Controllers;

use App\ForumCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ForumController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ForumCategory::all();

        return view('forum.home',[
            'categories'  => $categories
        ]);
    }

}
