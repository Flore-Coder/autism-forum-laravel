<?php

namespace App\Http\Controllers;

use App\ForumCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ForumCategoryController extends Controller
{
    private function palette(){
        $colornames = array(
            "#f9f1f1"=>"Blanc",
            "#dddddd" =>"Gris 1",
            "#cccccc" =>"Gris 2",
            "#ffb3ba"=>"Rose",
            "#ffdfba"=>"Orange",
            "#ffffba"=>"Jaune",
            "#baffc9"=>"Turquoise",
            "#bae1ff"=>"Bleu");
        return $colornames ;
    }

    public function index(){
        return view('forum.home')->with('categories', ForumCategory::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $colornames = $this->palette();
        return view('categories.create',[
            'colornames' => $colornames
        ]);
    }


    /*    public function store(Request $request)
        {
               $this->validate($request,
            [
                'category' => 'required'
            ]);

            ForumCategory::create([
            'title'=> $request->category
            ]);
            Session::flash('succes', 'Catégorie créée');
            return redirect()->route('forum.home');
        }*/

    public function store(Request $request)
    {
        $this->validate($request,
            [
                'category' => 'required'
            ]);

        $category = new ForumCategory();
        $category->title = $request->input('title');
        $category->description = $request->input('description');
        $category->isPrivate = $request->input('isPrivate');
        $category->color = $request->input('color');
        DB::insert('insert into forum_categories (title, description, color, is_private) values (?, ?, ?, ?)', [
            $category->title,
            $category->description,
            $category->color,
            $category->isPrivate,

        ]);
        Session::flash('succes', 'Catégorie créée');

        return redirect()->route('forum.home');;
    }

    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $category = ForumCategory::find($id);
        $colornames = $this->palette();

        return view('categories.edit',[
            'category' => $category,
            'colornames' => $colornames
        ]);
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);
        $category = ForumCategory::find($id);;

        $category->title = $request->input('title');
        $category->description = $request->input('description');
        $category->color = $request->input('color');

        $category->save();

        Session::flash('success', 'Catégorie modifiée');

        return redirect()->route('forum.home');
    }

    public function destroy($id)
    {
        ForumCategory::destroy($id);
        Session::flash('succes','La catégorie a bien été supprimée.');
        return redirect()->route('forum.home');
    }

}
