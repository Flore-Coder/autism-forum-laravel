<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'alias' => 'Flo',
            'avatar' => 'img/placeholder.jpg',
            //'avatar' => asset('avatars/avatar.png');
            'name' => 'Admin',
            'email' => 'f-pares@protonmail.com',
            'password' => bcrypt('12345678'),
            'role_id' => '1',
        ]);

        DB::table('users')->insert([
            'alias' => 'Georgio',
            'avatar' => 'img/placeholder.jpg',
            'name' => 'User',
            'email' => 'user@gmail.com',
            'password' => bcrypt('12345678'),
            'role_id' => '2',

        ]);

        DB::table('users')->insert([
            'alias' => 'Mina',
            'avatar' => 'img/placeholder.jpg',
            'name' => 'Moderateur',
            'email' => 'manager@gmail.com',
            'password' => bcrypt('12345678'),
            'role_id' => '3',

        ]);
    }
}
