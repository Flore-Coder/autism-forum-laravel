<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'role_name' => 'Administrator',
            'role_slug' => 'administrator',
        ]);

        DB::table('roles')->insert([
            'role_name' => 'Redactor',
            'role_slug' => 'redactor',
        ]);

        DB::table('roles')->insert([
            'role_name' => 'Moderator',
            'role_slug' => 'moderator',
        ]);

        DB::table('roles')->insert([
            'role_name' => 'StandardUser',
            'role_slug' => 'standard-user',
        ]);


    }
}
