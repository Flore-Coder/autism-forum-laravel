<?php

use App\ForumCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ForumCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forum_categories')->insert([
            'title' => 'Pour la modération',
            'description' => 'Echanges entre membres ayant les droits de modérateur',
            'color' => '#f9f1f1',
        ]);
        DB::table('forum_categories')->insert([
            'title' => 'Annonces',
            'description' => 'Les annonces pour appels à correspondants, informer sur les animations en relation avec l\'autisme
            par chez vous',
            'color' => '#dddddd',
        ]);

        DB::table('forum_categories')->insert([
            'title' => 'Pour les membres TSA dont les aspys ',
            'description' => 'Fils de discussions des personnes ayant 1 Trouble du Spectre Autistique, dont les aspys voulant
            échanger sur leur expérience et des particularités de leur autisme',
            'color' => '#bae1ff'
        ]);
        DB::table('forum_categories')->insert([
            'title' => 'Pour les membres de la famille d\'une personne autiste',
            'description' => 'Les parents d\'un enfant autiste ou collatéraux ou enfants d\'une personne autiste sont invités
            à s\'entre-aider sur ce forum.',
            'color' =>'#ffffba'
        ]);

    }

}
