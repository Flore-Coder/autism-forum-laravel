<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ForumMessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forum_messages')->insert([
            'user_id' => '3',
            'thread_id' => '1',
            'content' => 'Dans la famille blablabla dernière à être diagnostiquée blabla.'
        ]);
        DB::table('forum_messages')->insert([
            'user_id' => '2',
            'thread_id' => '1',
            'content' => 'Blabla ça m\'a aidé de trouver des gens comme moi blabla.'
        ]);
        DB::table('forum_messages')->insert([
            'user_id' => '3',
            'thread_id' => '2',
            'content' => 'Blabla pression blabla aucune aide financière.'
        ]);
        DB::table('forum_messages')->insert([
            'user_id' => '3',
            'thread_id' => '2',
            'content' => 'Blabla j\'ai compris blabla sensibilité blabla trop d\'empathie
            blabla fini par trouver les bons amis blabla pas toujours aussi bien au quotidien.'
        ]);
        DB::table('forum_messages')->insert([
            'user_id' => '6',
            'thread_id' => '3',
            'content' => 'Blabla il faut pas hésiter à voir si le courant passe blabla. Blabla
                    bla.'
        ]);
        DB::table('forum_messages')->insert([
            'user_id' => '4',
            'thread_id' => '3',
            'content' => 'Blabla présent aux séances blablabla pas facile de comprendre blabla.'
        ]);
    }
}
