<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ForumThreadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('forum_threads')->insert([
            'user_id' => '1',
            'title' => 'Dans quelles circonstances avez-vous pu être diagnostiqué(e) ?',
            'content' => 'Tout blablabla, blablabla met la puce à l\'oreille.'
        ]);
        DB::table('forum_threads')->insert([
            'user_id' => '2',
            'title' => 'Pensez-vous que les test HPI sont fiables ?',
            'content' => 'J\'hésite à passer le test blabla coûteux blablabla
            stigmatisé.'
        ]);


        DB::table('forum_threads')->insert([
            'user_id' => '3',
            'title' => 'Avez-vous entendu parler de violences envers les enfants autistes
            dans les centres ?',
            'content' => 'Je sais qu\'il y avait blablabla a fait régresser mon fils. Finalement
            blablabla. Je ne voudrais pas généraliser mais je n\'ose plus blablabla expérience.'
        ]);
    }
}
