<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*Route::get('/', function (){
    return view('forum.home');
});*/


Auth::routes();

Route::get('/', 'ForumController@index')->name('forum.home');
/*Route::get('/categories/create', 'ForumController@create')->name('categories.create');
Route::POST('/categories/store', 'ForumController@store')->name('categories.store');
Route::get('home', 'HomeController@index')->name('home');*/
Route::resource('categories', 'ForumCategoryController');
Route::resource('messages', 'ForumThreadController');
Route::get('/threads', 'ForumCategoryController@index')->name('threads.index');

/* Dashboard*/
/*
Route::middleware(['auth'])->group( function(){
    Route::get('home', 'HomeController@index')->name('home');
    Route::resource('categories', 'ForumCategoryController');
    Route::resource('messages', 'ForumThreadController');

});
*/
/*Route::get('/', HomeController@index);*/

/*{
    if(Auth::check() && Auth::user()->role_id == 1) {*/
/*        return redirect('administrator/dashboard');*/
/*        return view('administrator/dashboard');

    }
    elseif(Auth::check() && Auth::user()->role_id == 2){
/*        return redirect('redactor/dashboard');*/
/*        return view('redactor/dashboard');

    }elseif(Auth::check() && Auth::user()->role_id == 3){*/
/*        return redirect('moderator/dashboard');*/
/*        return view('moderator/dashboard');

    }elseif(Auth::check() && Auth::user()->role_id == 4) {*/
/*        return redirect('standard-user/dashboard');*/
//Route::get('/', 'HomeController@index')->name('home');
/*
group(['as'=>'administrator.','prefix' => 'administrator','namespace'=>'Administrator','middleware'=>['auth','administrator']], function () {
    Route::get('administrator', 'AdministratorController@index')->name('administrator');
});

Route::group(['as'=>'redactor.','prefix' => 'redactor','namespace'=>'redactor','middleware'=>['auth','redactor']], function () {
    Route::get('redactor', 'RedactorController@index')->name('redactor');
});


Route::group(['as'=>'moderator.','prefix' => 'moderator','namespace'=>'Moderator','middleware'=>['auth','moderator']], function () {
    Route::get('moderator', 'ModeratorController@index')->name('moderator');
});
Route::group(['as'=>'standard-user.','prefix' => 'standard-user','namespace'=>'StandardUser','middleware'=>['auth','standard-user']], function () {
    Route::get('standard-user', 'StandardUserController@index')->name('standard-user');
});

*/
/*Route::group(['middleware' => 'auth'], function(){
   Route::resource('ForumThread', 'ForumThreadController');
});*/
/*
Route::group(['middleware' => 'auth'], function(){
    Route::resource('ForumCategory', 'ForumCategoryController');
});
*/
